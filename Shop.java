import java.util.Scanner;

/*
 this is a subway shop, i will ask some basic ingredients they want, 
 then tell them what they ordered, the price and tip
 */
public class Shop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Kitchen[] sandwich = new Kitchen[4];

        System.out.println("Hi, welcome to subway the four of you, well let's start...  ");

        for (int i = 0; i < sandwich.length; i++) {
            sandwich[i] = new Kitchen();
            System.out.println(" ");

            System.out.println("customer number " + (i + 1) + ", for bread, do you want white or italien?");
            sandwich[i].bread = scan.next();

            System.out.println("Do you want chicken, meatball or lamb?");
            // never saw vegeterian subway so not gonna bother with that
            sandwich[i].protein = scan.next();

            System.out.println("Do you want vegetables? yes or no");
            sandwich[i].veggy = Character.toLowerCase(scan.next().charAt(0));

            System.out.println("Do you want cookies? yes or no");
            char cookieOrNot = Character.toLowerCase(scan.next().charAt(0));
            if (cookieOrNot == 'y') {
                System.out.println("how many cookies do you want?");
                sandwich[i].cookies = scan.nextInt();
            } else {
                sandwich[i].cookies = 0;
            }
        }

        System.out.println(" ");
        System.out.println("Alraight! your orders are ready, come pay here: ");

        for (int i = 0; i < sandwich.length; i++) {
            System.out.println(" ");
            System.out.println("For customer number " + (i + 1) + ", your order is " + sandwich[i].sandwichMaker());
            // formatting the double type to make it have only 2 decimal places as float
            // type for presentation
            System.out.println("Your total is " + String.format("%.2f", sandwich[i].priceMaker()) + "$");
            System.out.println("Thank you for your generous tip of " + String.format("%.2f", sandwich[i].randomTip()));
            // we gotta be kind to all customers, even if they give shitty tips
        }
        scan.close();
    }
}
