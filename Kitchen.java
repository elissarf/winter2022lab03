import java.util.Random;

public class Kitchen {
    public String bread;
    public String protein;
    public char veggy;
    public int cookies;
    // making a field that is calculated by other fields in method priceMaker
    // and used in another method randomTip
    public double price;

    public String sandwichMaker() {
        // make a string that will print all the fields
        String sandwichName = "a " + this.protein + " sandwich with " + this.bread + " bread, ";
        if (veggy == 'n') {
            sandwichName = sandwichName + "no ";
        }
        sandwichName = sandwichName + "vegetables, and " + this.cookies + " cookies.";
        return sandwichName;
    }

    public double priceMaker() {
        // method for price of the sandwich
        // make the string fields easier to use by converting to char
        char proteinType = Character.toLowerCase(this.protein.charAt(0));
        char breadType = Character.toLowerCase(this.bread.charAt(0));
        // setting the null value to 0
        price = 0;

        // price for protein
        switch (proteinType) {
            case 'l':
                price = price + 12.49;
                break;
            case 'c':
                price = price + 6.99;
                break;
            case 'm':
                price = price + 8.49;
                break;
        }
        // price for bread
        if (breadType == 'i') {
            price = price + 3.39;
        } else if (breadType == 'w') {
            price = price + 2.19;
        }
        // price for veggy
        if (this.veggy == 'y') {
            price = price + 2.19;
        }
        // price for cookies
        price = price + (this.cookies * 2.49);

        return price;
    }

    public double randomTip() {
        Random random = new Random();
        // making random tip of maximum 50% of the final price
        // max tip would prob be 50%
        double percetage = random.nextInt(51);
        double tip = percetage / 100 * this.price;
        return tip;
    }

}
